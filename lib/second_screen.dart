import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'fetch_data/fetch_todo.dart';

class MySecondScreen extends StatelessWidget {
  final data = Get.find<FetchTodo>();
  @override
  Widget build(BuildContext context) {
    // try {
    //   final data = Get.find<FetchTodo>();
    // } catch (e) {
    //   print(e.toString());
    // }
    print(Get.parameters['device']);
    print(Get.parameters['id']);
    print(Get.parameters['name']);
    return Scaffold(
      appBar: AppBar(
        title: Text("Second"),
      ),
      body: Container(
        child: Center(
            child: InkResponse(
                onTap: () {
                  Timer(Duration(seconds: 5), () {
                    Get.toNamed("/profile/1");
                  });

                  Get.offNamed("/third", arguments: {"name": "OK"});
                },
                child: Text(data.data[0].title))),
      ),
    );
  }
}
