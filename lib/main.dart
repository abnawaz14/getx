import 'package:flutter/material.dart';
import 'package:flutter_getx/second_screen.dart';
import 'package:flutter_getx/thirdscreen.dart';
import 'package:get/get.dart';

import 'UserProfile.dart';
import 'fetch_data/fetch_Data.dart';
import 'fetch_data/fetch_todo.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialBinding: BindingsBuilder(() {
        Get.put<FetchData>(FetchData());
        Get.put<FetchTodo>(FetchTodo());
      }),
      routingCallback: (routing) {
        print(routing.current);

        if (routing.current == '/third') {
          // if (!Get.isSnackbarOpen) {   //this will run multiple times
          //   Get.snackbar("Hi", "You are on second route",
          //       instantInit:
          //           false); //instantInit make it to run before initState.
          // }
        }
      },
      unknownRoute: GetPage(name: "/notfound", page: () => MyHomePage()),
      initialRoute: "/first",
      getPages: [
        GetPage(name: "/first", page: () => MyHomePage()),
        GetPage(
          name: "/second",
          page: () => MySecondScreen(),
          transition: Transition.zoom,
        ),
        GetPage(name: "/third", page: () => ThirdScreen()),
        GetPage(
          name: '/profile/:user',
          page: () => UserProfile(),
        ),
      ],
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
            child: InkResponse(
                onTap: () {
                  Get.toNamed("/second?device=phone&id=354&name=Enzo");
                },
                child: Text("First"))),
      ),
    );
  }
}
