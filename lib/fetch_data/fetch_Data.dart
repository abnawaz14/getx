import 'package:dio/dio.dart';
import 'package:flutter_getx/model/json_data.dart';

class FetchData {
  List<JsonData> data = [];
  FetchData() {
    fetchData();
  }

  fetchData() async {
    Response res =
        await Dio().get('https://jsonplaceholder.typicode.com/posts');
    for (Map<String, dynamic> r in res.data) {
      data.add(JsonData.fromJson(r));
    }
  }
}
