import 'package:dio/dio.dart';
import 'package:flutter_getx/model/todo_data.dart';

class FetchTodo {
  List<TodoData> data = [];
  FetchTodo() {
    fetchData();
  }
  fetchData() async {
    Response res =
        await Dio().get('https://jsonplaceholder.typicode.com/todos');
    print(res.data);
    for (Map<String, dynamic> r in res.data) {
      data.add(TodoData.fromJson(r));
    }
    return data;
  }
}
