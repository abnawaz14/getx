import 'package:flutter/material.dart';
import 'package:flutter_getx/fetch_data/fetch_Data.dart';
import 'package:get/get.dart';

class UserProfile extends StatelessWidget {
  final data = Get.find<FetchData>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Get.parameters['user']),
      ),
      body: Container(
        child: Center(
            child: InkResponse(
                onTap: () {
                  Get.offAllNamed("/first");
                },
                child: Text(data.data[0].title))),
      ),
    );
  }
}
